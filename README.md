# Polynomial primality test AKS

My work in based on this paper: https://www.cse.iitk.ac.in/users/manindra/algebra/primality_v6.pdf.

The project report is compte_rendu.pdf (in french). The presentation is in presentation.pdf.

Experimental data (figure_1.png) has been tested without openmp (parallelism is implemented in the code though, resulsts hasn't been updated):

![](https://gitlab.com/s1rr/test-de-primalite-polynomial-aks/-/raw/main/Figure_1.png)
