#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include <omp.h>
#include <gmp.h>
#include "polynomes.c"
#include "test.c"
#include "main.h"
#include "performances.c"


int main (int argc, char* argv[]) {
  /* mpz_t testp; */
  /* mpz_init_set_ui(testp, 31); */
  aks(N0);
  /* for (int i = 4; i <= 50; i++) { */
  /*   aks(testp); */
  /*   mpz_add_ui(testp, testp, 1); */
  /* } */
  // aks_speed_data();
  // int aks_test = aks(N3);
  // printf("\nresult: %d\n", aks_test);
  return 0;
}



// int aks(mpz_t prime) {
int aks(char n_in[]) {
  mpz_t n;
  mpz_t r;
  mpz_init(n);
  mpz_init(r);
  mpz_set_str(n, n_in, 10);
  // mpz_set(n, prime);
  printf("---------------- n: ");
  mpz_out_str(stdout, 10, n);
  printf("\n");

  if (mpz_perfect_power_p (n)) { // 1) On test si n est une puissance parfaite
    printf("Etape 1: n in a perfect square. n COMPOSITE.\n");
    mpz_clear(n);
    mpz_clear(r);
    return 0;
  }

  r_research(n, r); // 2) Trouver le plus petit r tq o_r(n) > log^2(n)

  int test_inter = test_intermediaire(n, r); // 3) 4) on vérifie que n n'a aucun diviseur plus petit que min(r, n-1), et si n<=r
  if (test_inter == 0) { // composite
    mpz_clear(n);
    mpz_clear(r);
    return 0;
  } else if (test_inter == 1) { // prime
    mpz_clear(n);
    mpz_clear(r);
    return 1;
  }

  /* int lenstra = lenstra_variant(n, r); */
  /* if (lenstra == 0) { // composite */
  /*   mpz_clear(n); */
  /*   mpz_clear(r); */
  /*   return 0; */
  /* } else if (lenstra == 1) { // prime */
  /*   mpz_clear(n); */
  /*   mpz_clear(r); */
  /*   return 1; */
  /* } */


  int test_f = test_final(n, r); // 5)
  if (test_f) { // prime
    mpz_clear(n);
    mpz_clear(r);
    return 1;
  } else if (test_f == 0) { // composite
    mpz_clear(n);
    mpz_clear(r);
    return 0;
  }
  mpz_clear(n);
  mpz_clear(r);
  return 0;
}

double long my_log2(mpz_t n) {
  // https://stackoverflow.com/questions/11432436/is-there-any-gmp-logarithm-function
  // https://gmplib.org/manual/Integer-Internals

  // log(a/b) = log(a) - log(b)
  // Soit a = a_N B^N + a_{N-1} B^{N-1} + ... + a_0.
  // On a log(a) ~ log(a_N B^N) = log(a_N) + N log(B), ou
  // B = ULONG_MAX

  double long log_base = log2(ULONG_MAX); // on considère ici que la taille des limbs est __int64 ou long long
  double long log2n;

  // undefined log // ex: mpz = [0, 1]
  // https://www.gnu.org/software/libc/manual/html_node/Infinity-and-NaN.html
  /* if (mpz_get_ui(n->_mp_d[nb_limbs - 1]) == 0) { */
  /*   return INFINITY; */
  /* } */

  int nb_limbs = abs(n->_mp_size);
  log2n = log2(n->_mp_d[nb_limbs - 1]) + (nb_limbs - 1)*log_base;

  return log2n;
}


/* https://cp-algorithms.com/algebra/phi-function.html */
/* r étant finalement petit, cette fonction peut être implémenter sans gmp */
int my_phi(mpz_t input, mpz_t a) { // Memory leak: 1 bloc perdu par appel dans mpz_sub
  mpz_t k;
  mpz_t sqrta;
  mpz_t tmp;
  mpz_t mod_ak;
  mpz_init(k);
  mpz_init(sqrta);
  mpz_init(tmp);
  mpz_init(mod_ak);
  mpz_set(input, a);
  mpz_set_ui(k, 2);
  mpz_set_ui(tmp, 1);
  mpz_sqrt(sqrta, a);
  while (mpz_cmp(k, sqrta) <= 0) {
    // printf("cmp: %d\n", mpz_cmp(k, sqrta));
    mpz_gcd(sqrta, sqrta, k);
    mpz_mod(mod_ak, a, k);
    if (mpz_cmp_ui(mod_ak, 0) == 0) { // if (a % k == 0)
      while (mpz_cmp_ui(mod_ak, 0) == 0) {
        mpz_div(a, a, k);
        mpz_mod(mod_ak, a, k);
      }
      mpz_div(tmp, input, k); // input = input - input/k;
      mpz_sub(input, input, tmp);
    }
    mpz_add_ui(k, k, 1);
  }
  if (mpz_cmp_ui(a, 1) > 0) {
    mpz_div(tmp, input, a); // input = input - input/a;
    mpz_sub(input, input, tmp);
  }

  printf("phi_r: ");
  mpz_out_str(stdout, 10, input);
  printf("\n");

  mpz_clear(k);
  mpz_clear(sqrta);
  mpz_clear(tmp);
  mpz_clear(mod_ak);
  return 0;
}

/* https://cp-algorithms.com/algebra/sieve-of-eratosthenes.html */
int crible_eratosthene(int is_prime[], int limit) {
  is_prime[0] = is_prime[1] = false;
  for (int i = 2; i <= limit; i++) {
    if (is_prime[i] && (long long)i * i <= limit) {
      for (int j = i * i; j <= limit; j += i)
        is_prime[j] = false;
    }
  }
  return 0;
}

/* 2) Trouver le plus petit r tq o_r(n) > log^2(n) */
/*    On essaie les valeurs successives de r et on valide r si n^k =/= 1 (mod r) pour tout k * <= log^2(n). Par le lemme 4.3, r est plus petit que max(3, int(log^5(n))
      If r and n are not coprime, then skip this r */
int r_research(mpz_t n, mpz_t r) {
  mpz_t nkmodr;
  mpz_t pgcdnr;
  mpz_t mpzrtest;
  mpz_init(nkmodr);
  mpz_init(pgcdnr);
  mpz_init(mpzrtest);
  double long logn = my_log2(n);
  double long limr = fmax(3, pow(logn, 5));
  int k_test = true;

  for (int rtest = 2; rtest < limr; rtest++) {
    mpz_gcd_ui(pgcdnr, n, rtest);
    mpz_set_ui(mpzrtest, rtest);
    k_test = true;

    if (mpz_cmp_ui(pgcdnr, 1) == 0) { // r doit etre prem avec n car ord mult
      for (int k = 1; k <= logn * logn; k++) {
        mpz_powm_ui(nkmodr, n, k, mpzrtest);

        if (mpz_cmp_ui(nkmodr, 1) == 0)
          k_test = false;
      }
      if (k_test == true) {
        mpz_set_ui(r, rtest);
        printf("r: ");
        mpz_out_str(stdout, 10, r);
        printf("\n");
        mpz_clear(nkmodr);
        mpz_clear(pgcdnr);
        mpz_clear(mpzrtest);
        return 0;
      }
    }
  }
  mpz_clear(nkmodr);
  mpz_clear(pgcdnr);
  mpz_clear(mpzrtest);
  return 0;
}



/* 3) 4) on vérifie que n n'a aucun diviseur plus petit que min(r, n-1), et si n<=r */
/* For all 2 ≤ a ≤ min (r, n−1), check that a does not divide n: If a|n for some 2 ≤ a ≤ min (r, n−1), output composite. */
/* if ( pgcd(a, n) > 1 et n > pgcd(a, n) pour tout a <= r)  alors COMPOSITE */
/*    On calcule pgcd(a, n) au plus r fois. */
int test_intermediaire(mpz_t n, mpz_t r) {
  mpz_t pgcd_kn;
  mpz_init(pgcd_kn);
  uint k = 2;
  while (mpz_cmp_ui(r, k) > 0) {
    mpz_gcd_ui(pgcd_kn, n, k);
    if ((mpz_cmp_ui(pgcd_kn, 1) > 0) && (mpz_cmp(pgcd_kn, n) < 0)) {
      printf("Etape 3: test inter: n COMPOSITE.\n");
      mpz_clear(pgcd_kn);
      return 0;
    }
    k++;
  }
  mpz_clear(pgcd_kn);

  if (mpz_cmp(n, r) <= 0) {
    printf("Etape 4: test inter n<=r: n PRIME.\n");
    return 1;
  }
  return 2; // 2 =/= 1 or 0 for continue
}


/* 5) for a == 1 to int(sqrt(phi(r)) * log(n)):
   if ( (X+a)^n =/= X^n + 1 (mod X^r - 1, n)), alors COMPOSE
   On calcule: (X + a)^n (mod X^r − 1, n)
   X^n + a (mod X^r − 1, n)
   puis on compare */
int test_final(mpz_t n, mpz_t r) {
  uint lim;
  mpz_t nmodr;
  mpz_t phir;
  mpz_init(nmodr);
  mpz_init(phir);
  uint uint_r = mpz_get_ui(r); // uint_r est mpz r en uint
  assert(uint_r > 0); // probleme avec n = 2
  mpz_mod(nmodr, n, r);
  uint uint_nmodr = mpz_get_ui(nmodr);

  // sqrt(phi(r)) * log(n)
  my_phi(phir, r);
  lim = sqrt(mpz_get_ui(phir)) * my_log2(n);

  printf("lim: %d", lim);
  printf("\n\n");

  #ifdef USE_PARALLEL
  uint i = 0;
  uint flag = false;
  for (i = 0; i <= (lim/THREAD_NUM) - 1; i++) {
    #pragma omp parallel for
    for (int j = 1; j <= THREAD_NUM; j++) {
      uint a = i * THREAD_NUM + j;
      pol *P_gauche;
      pol *P_droite;
      pol_init(&P_gauche, uint_r - 1);
      pol_init(&P_droite, uint_r - 1); // pol droite: X^(n mod r) + a, a doit etre mod n
      mpz_set_ui(P_droite->coefs[uint_nmodr], 1);
      mpz_set_ui(P_droite->coefs[0], a);
      exponentiation_modulaire_aks(P_gauche, n, uint_r, a);
      printf("Pol p droite:    "); // DEBUG //
      print_pol(*P_droite);        // DEBUG //
      printf("\n");                // DEBUG //
      if (pol_cmp(P_gauche, P_droite) == 0) {
        flag = true;
      }
      pol_clear(&P_gauche);
      pol_clear(&P_droite);
    }
    if (flag) {
      printf("Etape 5: (X + a)^n mod (X^r - 1, n) = (X^n + a) égale. COMPOSITE\n");
      mpz_clear(nmodr);
      return 0;
    }
  }
  #endif
  pol* P_gauche; // on exécute les derniers calculs
  pol *P_droite;
  pol_init(&P_gauche, uint_r - 1);
  pol_init(&P_droite, uint_r - 1); // pol droite: X^(n mod r) + a, a doit etre mod n
  mpz_set_ui(P_droite->coefs[uint_nmodr], 1);
  #ifdef USE_PARALLEL
  for (i = lim - THREAD_NUM; i <= lim; i++) {
  #endif
  #ifndef USE_PARALLEL
  for (int i = 1; i <= lim; i++) {
  #endif
    exponentiation_modulaire_aks(P_gauche, n, uint_r, i);
    mpz_set_ui(P_droite->coefs[0], i);
    printf("Pol p droite:    "); // DEBUG //
    print_pol(*P_droite); // DEBUG //
    printf("\n"); // DEBUG //
    if (pol_cmp(P_gauche, P_droite) == 0) {
      printf("Etape 5: (X + a)^n mod (X^r - 1, n) = (X^n + a) égale. COMPOSITE\n");
      pol_clear(&P_gauche);
      pol_clear(&P_droite);
      mpz_clear(nmodr);
      return 0;
    }
    pol_zero(P_gauche);

  }
  pol_clear(&P_gauche);
  pol_clear(&P_droite);
  printf("Etape 5: (X + a)^n mod (X^r - 1, n) != (X^n + a) pour tout a. PRIME\n\n");
  mpz_clear(nmodr);
  return 1;
}



/* Implémentation de la variante de Lenstra */
/* 2. Calculer $N = 2n(n-1)(n^2-1)...(n^{4lg(n)}^2 - 1)$. Cherche le plus petit r qui ne divise pas N.\\ \\ */
/* A cette étape 2, la multiplication de N n'est pas faisable car il peux vite */
/* devenir trop grand. Pour y remédier, on crée un tableau ou on stocke [2, n, */
/* n-1, n^2 -1, ...]. Pour savoir si r ne divise pas N, on va calculer le pgcd */
/* de r_test et N[0], puis on divise r_test par le pgcd, et on répète pour tout */
/* i. A la fin de la boucle, si r_test = 1, alors c'est que r divise N. */

/* 3. Si n est égale à un nombre premier plus petit que r, rendre COMPOSITE\\ \\ */
/* 4. Si n est divisible par un nombre premier plus petit que r, rendre COMPOSITE\\ \\ */
int lenstra_variant(mpz_t n, mpz_t r) {
  mpz_t N_tmp;
  mpz_t gcd_Nr;
  mpz_t r_tmp;
  uint k = 1;
  mpz_t *N;
  mpz_init(N_tmp);
  mpz_init(r_tmp);
  mpz_init(gcd_Nr);
  uint limit;

  int logn = (int)my_log2(n) + 1; // 4{lg(n)}^2
  limit = 4*pow(logn, 2);
  /* printf("\nlogn: %d", logn); */
  /* printf("\nlimit: %d", limit); */

  N = (mpz_t *) malloc((limit + 2) * sizeof(mpz_t)); // init of N
  for (int i = 0; i < (limit + 2); i++) {
    mpz_init(N[i]);
  }

  mpz_set_ui(N[0], 2);
  mpz_set(N[1], n);
  while (limit >= k) { // On calcule N
    mpz_set(N[k], n);
    mpz_pow_ui(N[k], N[k], k);
    mpz_sub_ui(N[k], N[k], 1); // n^i - 1
    k++;
  }
  // if (mpz_divisible_p(N[i], r) == 0) { // N divisible par r // Non-zero if n is exactly divisible by d.

  mpz_set_ui(r, 2);
  while (mpz_cmp_ui(r, limit) < 0) { // On cherche r
    mpz_set(gcd_Nr, r);
    mpz_set(r_tmp, r);
    for (int i = 0; i < (limit + 2); i++) {
      mpz_gcd(gcd_Nr, r_tmp, N[i]);
      mpz_div(r_tmp, r_tmp, gcd_Nr);
    }
    if (mpz_cmp_ui(r_tmp, 1) != 0) { // si r_tmp != 1
        limit = 0; // on arrete la boucle
    }
    mpz_add_ui(r, r, 1);
  }

  printf("\nr: ");
  mpz_out_str(stdout, 10, r);
  printf("\n");


  mpz_set_ui(N_tmp, 3); // on réutilise cette variable
  int is_prime[mpz_get_ui(r)];
  crible_eratosthene(is_prime, mpz_get_ui(r));
  if (mpz_cmp(n, r) < 0) {
    while (mpz_cmp(N_tmp, r) < 0) { // n divisible par nb premier plus petit que r
      if (is_prime[mpz_get_ui(N_tmp)]) {
        if (mpz_cmp(n, N_tmp)) // n égale a N_tmp premier
          return 1;
        if (mpz_divisible_p(n, N_tmp) == 0) // N_tmp premier divisible pas n
          return 0; // composite
      }
      mpz_add_ui(N_tmp, N_tmp, 1);
    }
  }

  free(N);
  mpz_clear(N_tmp);
  return 2;
}
