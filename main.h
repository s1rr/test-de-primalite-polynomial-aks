/* main.h */


#define true 1
#define false 0

typedef unsigned int uint;

#define USE_PARALLEL
#define THREAD_NUM 8

#define N0 "4099" // ordre 2^12
#define N1 "32771"  // ordre 2^15
#define N2 "131101" // ordre 2^17
#define N3 "1048583" // ordre 2^20
#define N4 "33554467" // ordre 2^25
#define N5 "1073741827" // ordre 2^30

// int aks(mpz_t prime);
int aks(char n_in[]);
int r_research(mpz_t n, mpz_t r);
int test_intermediaire(mpz_t n, mpz_t r);
int test_final(mpz_t n, mpz_t r);
double long my_log2(mpz_t n);
int my_phi(mpz_t input, mpz_t a);
int lenstra_variant(mpz_t n, mpz_t r);
int crible_eratosthene(int is_prime[], int limit);


// valgrind --leak-check=yes --track-origins=yes ./main
// gcc -g -Wall -lgmp -lm main.c -o main
