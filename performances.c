



// On génère un nombre d'ordre 2^i, puis on utilise une fonction interne à GMP
// pour trouver le nombre premier suivant. On intère sur i pour tester
// l'algorithme sur des nombres premiers d'ordre de grandeur croissant. On
// stocke les données dans un fichier data.txt, et on génère le graphique avec
// un script python.
// https://stackoverflow.com/questions/10874214/measure-execution-time-in-c-openmp-code
int aks_speed_data() {
  mpz_t compteur; // augmente de *2
  mpz_t prime;
  clock_t start, end; // https://www.geeksforgeeks.org/how-to-measure-time-taken-by-a-program-in-c/
  double cpu_time;
  int prime_t = 0;
  mpz_init_set_ui(compteur, 2);
  mpz_init(prime);
  FILE *data_file = fopen("data.txt", "w"); // lecture et ecriture

  mpz_pow_ui(compteur, compteur, 15);  
  for (int i = 15; i <= 15; i++) {
    prime_t = 0;
    mpz_nextprime(prime, compteur); // prend le nb premier le plus proche de a
    start = omp_get_wtime(); // clock();
    // prime_t = aks(prime);
    end = omp_get_wtime(); // clock();
    cpu_time = end - start;
    // cpu_time = ((double)(end - start)) / CLOCKS_PER_SEC;
    // fprintf(data_file, "2^%d ", i);
    // mpz_out_str(data_file, 10, prime);
    fprintf(data_file, "%d %f\n", i, cpu_time);
    // fprintf(data_file, " %f %d \n", cpu_time, prime_t); // si on veut plus de données
    mpz_mul_ui(compteur, compteur, 2);
  }

  fclose(data_file);
  mpz_clear(compteur);
  mpz_clear(prime);
  return prime_t;
}
