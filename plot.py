import matplotlib.pyplot as plt
import numpy as np

data = np.loadtxt("data.txt")

plt.plot(data[:, 0], data[:, 1])
plt.xlabel('2^i')
plt.ylabel('time in seconds')
plt.title("Graphe")
plt.show()
# plt.savefig("graph.png")

