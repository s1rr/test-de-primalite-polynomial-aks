/* polynomes.c */

#include "polynomes.h"

/* Pour notre usecase, r suffisament petit (<= log^5(n)) pour nous permettre d'utiliser
   uint pour deg au lieu de mpz_t */

/* helper function exponentiation modulaire */
int exponentiation_modulaire_aks(pol* P_dest, mpz_t n, uint r, uint a) {
  pol* P_res;
  pol_init(&P_res, r - 1);
  mpz_set_ui(P_res->coefs[0], a);
  mpz_set_ui(P_res->coefs[1], 1);
  printf("P avant exp mod: "); // DEBUG //
  print_pol(*P_res); // DEBUG //
  pol_exponentiation_modulaire(P_dest, P_res, n, r);
  printf("Pol p gauche:    "); // DEBUG //
  print_pol(*P_dest); // DEBUG //
  pol_clear(&P_res);
  return 0;
}


/* On initialize un pointeur vers la structure pol */
/* **P est la premiere val de la struct, *P est l'addresse de la structure et */
/* pointe vers le premier élément de la structure, P pointe vers l'addresse de */
/* la structure */
int pol_init(pol** P, uint deg) {
  *P = (pol*)malloc(sizeof(pol)); // on init la memoire pour uint et mpz_t* les elts de la struct
  // (**P).coefs est un pointeur par def de la structure, qui pointe pour l'instant vers rien
  (**P).coefs = malloc(sizeof(mpz_t) * (deg + 1));
  (**P).deg = deg;
  for (int i = 0; i <= deg; i++) {
    mpz_init((**P).coefs[i]);
  }
  return 0;
}

int pol_clear(pol** P) {
  for (int i = 0; i < ((**P).deg + 1); i++) {
    mpz_clear((**P).coefs[i]);
  }
  free((**P).coefs);
  (**P).deg = 0;
  free(*P);
  return 0;
}

int pol_copy(pol* P_dest, pol* P_to_copy) {
  assert((*P_dest).deg >= (*P_to_copy).deg);
  (*P_dest).deg = (*P_to_copy).deg;
  for (int i = 0; i < ((*P_to_copy).deg + 1); i++) {
    mpz_set((*P_dest).coefs[i], (*P_to_copy).coefs[i]);
  }
  return 0;
}

/* On assigne à tous les coefficients 0 */
int pol_zero(pol* P) {
  for (int i = 0; i <= P->deg; i++) {
    mpz_set_ui(P->coefs[i], 0);
  }
  return 0;
}

/* Comparaison de P et Q, retourne 1 si egaux, 0 sinon */
int pol_cmp(pol* P, pol* Q) {
  for (int i = 0; i <= P->deg; i++) {
    if (mpz_cmp(P->coefs[i], Q->coefs[i]) != 0) {
      return 0;
    }
  }
  return 1;
}

/* Addition mod n entre deux polynomes P et Q */
int pol_add_mod(pol* P_dest, pol* P, pol* Q, mpz_t n){
  for (int i = 0; i <= P->deg; i++) {
    mpz_add(P_dest->coefs[i], P->coefs[i], Q->coefs[i]);
    mpz_mod(P_dest->coefs[i], P_dest->coefs[i], n);
  }
  return 0;
}

/* Multiplication mod n entre un polynome P et un coefficient */
int pol_mult_mod_coef(pol* P_dest, pol* P, mpz_t coef, mpz_t n) { // pas besoin de P_res intermediaire ici
  for (int i = 0; i <= P_dest->deg; i++) {
    mpz_mul(P_dest->coefs[i], P->coefs[i], coef);
    mpz_mod(P_dest->coefs[i], P_dest->coefs[i], n);
  }
  return 0;
}

/* Multiplication mod n entre un polynome P et un monome X^coef_monome */
int pol_mult_mod_monome(pol* P_dest, pol* P, uint coef_monome, uint r) {
  pol* P_res;
  pol_init(&P_res, r - 1);
  for (int i = 0; i <= P_dest->deg; i++) {
    if (i + coef_monome <= P_dest->deg) {
      mpz_set(P_res->coefs[i + coef_monome], P->coefs[i]);
    } else {
      mpz_set(P_res->coefs[(i + coef_monome) - (P_dest->deg + 1)], P->coefs[i]);
    }
  }
  pol_copy(P_dest, P_res);
  pol_clear(&P_res);
  return 0;
}

/* Multiplication modulaire de P et Q. On crée une variable P_tmp qui sera le */
/* résultat de P*ai*X^i, qu'on additionnera à P_dest considérer le polynome nul. */
int pol_multiplication_modulaire(pol* P_dest, pol* P, pol* Q, mpz_t n, uint r) {
  pol* P_tmp;

  pol_init(&P_tmp, r - 1);
  for (int i = 0; i <= Q->deg; i++) {
    pol_zero(P_tmp);
    if (mpz_cmp_ui(Q->coefs[i], 0) != 0) { // si coef = 0, on fait rien
      if (mpz_cmp_ui(Q->coefs[i], 1) == 0) { // si coef = 1, on copie directement le pol
        pol_copy(P_tmp, P);
      } else {
        pol_mult_mod_coef(P_tmp, P, Q->coefs[i], n);
      }
      if (i != 0) { // si i = 0, pas de rotation a faire
        pol_mult_mod_monome(P_tmp, P_tmp, i, r); // il faut que pol_mult_mod_coef soit tjs appelée sinon pb
      }
    }
    pol_add_mod(P_dest, P_dest, P_tmp, n);
  }

  pol_clear(&P_tmp);
  return 0;
}

/* Calcul de l'exponentiation modulaire de P puissance n. On adapte la méthode */
/* générale itérative aux polynomes. Dans cette version, on essaie de concentrer */
/* la création de polynomes ici plutôt que dans des sous fonctions pour éviter */
/* des créations et clears de polynomes inutiles */
int pol_exponentiation_modulaire(pol* P_dest, pol* P, mpz_t n, uint r) {
  assert(P_dest->deg == (r - 1)); // attention au deg de P_dest, je le considere de deg r - 1
  mpz_t mod_k2;
  pol* P_tmp;
  pol* P_tmp2;
  pol_init(&P_tmp, r - 1);
  pol_init(&P_tmp2, r - 1);
  mpz_t k;
  mpz_init(mod_k2);
  mpz_init_set(k, n);
  mpz_set_ui(P_dest->coefs[0], 1);
  assert(mpz_cmp_ui(k, 0) > 0); // on elimite les cas n <= 0
  while (mpz_cmp_ui(k, 1) >= 0) {
    mpz_mod_ui(mod_k2, k, 2);
    if (mpz_cmp_ui(mod_k2, 0) == 0) {
      mpz_div_ui(k, k, 2);
    } else {
      // On préfère faire des copies de pol ici plutot qu'apres pour ne pas
      // avoir a redefinir et reclear les polynomes à chaque appel de la mult
      pol_zero(P_tmp);
      pol_multiplication_modulaire(P_tmp, P_dest, P, n, r);
      pol_copy(P_dest, P_tmp);
      // pol_zero(P_tmp); pas besoin car copie en dessous
      mpz_sub_ui(k, k, 1); // n = (n - 1)/2
      mpz_div_ui(k, k, 2);
    }
    pol_copy(P_tmp, P);
    pol_zero(P_tmp2);
    pol_multiplication_modulaire(P_tmp2, P_tmp, P, n, r);
    pol_copy(P, P_tmp2);
  }
  pol_clear(&P_tmp);
  pol_clear(&P_tmp2);
  mpz_clear(mod_k2);
  mpz_clear(k);
  return 0;
}

/* Version plus simple ou nous faisons les copies de polynomes dans */
/* pol_multiplication_modulaire Plutot que dans l'exp rapide */
/* int pol_multiplication_modulaire(pol* P_dest, pol* P, pol* Q, mpz_t n, uint r) { */
/*   pol* P_res; */
/*   pol* P_tmp; */

/*   pol_init(&P_res, r - 1); */
/*   pol_init(&P_tmp, r - 1); */
/*   for (int i = 0; i <= Q->deg; i++) { */
/*     pol_zero(P_tmp); */
/*     if (mpz_cmp_ui(Q->coefs[i], 0) != 0) { // si coef = 0, on fait rien */
/*       if (mpz_cmp_ui(Q->coefs[i], 1) == 0) { // si coef = 1, on copie directement le pol */
/*         pol_copy(P_tmp, P); */
/*       } else { */
/*         pol_mult_mod_coef(P_tmp, P, Q->coefs[i], n); */
/*       } */
/*       if (i != 0) { // si i = 0, pas de rotation a faire */
/*         pol_mult_mod_monome(P_tmp, P_tmp, i, r); // il faut que pol_mult_mod_coef soit tjs appelée sinon pb */
/*       } */
/*     } */
/*     pol_add_mod(P_res, P_res, P_tmp, n); */
/*   } */

/*   pol_copy(P_dest, P_res); */
/*   pol_clear(&P_res); */
/*   pol_clear(&P_tmp); */
/*   return 0; */
/* } */

/* // calcul de l'exponentiation modulaire de P puissance n */
/* int pol_exponentiation_modulaire(pol* P_dest, pol* P, mpz_t n, uint r) { */
/*   assert(P_dest->deg == (r - 1)); // attention au deg de P_dest, je le considere de deg r - 1 */
/*   mpz_t mod_k2; */
/*   mpz_t k; */
/*   mpz_init(mod_k2); */
/*   mpz_init_set(k, n); */
/*   mpz_set_ui(P_dest->coefs[0], 1); */
/*   assert(mpz_cmp_ui(k, 0) > 0); // on elimite les cas n <= 0 */
/*   while (mpz_cmp_ui(k, 1) >= 0) { */
/*     mpz_mod_ui(mod_k2, k, 2); */
/*     if (mpz_cmp_ui(mod_k2, 0) == 0) { */
/*       mpz_div_ui(k, k, 2); */
/*     } else { */
/*       pol_multiplication_modulaire(P_dest, P_dest, P, n, r); // pb ici P_res est a 1 dans l'autre calcul */
/*       mpz_sub_ui(k, k, 1); // n = (n - 1)/2 */
/*       mpz_div_ui(k, k, 2); */
/*     } */
/*     pol_multiplication_modulaire(P, P, P, n, r); */

/*   } */
/*   mpz_clear(mod_k2); */
/*   mpz_clear(k); */
/*   return 0; */
/* } */

/* Convertie en bits un uint */
int print_bits_uint(uint n) {
  for (int i = 32; i >= 0; i--) {
    if ((n & (1 << i)) == 0)
      printf("0 ");
    else
      printf("1 ");
  }
  printf("\n");
  return 0;
}

/* Affiche le polynome P de manière lisible */
int print_pol(pol P) {
  uint exposant = 0;
  int plusswitch = 0;
  mpz_t coefprint;
  mpz_init(coefprint);
  printf("Pol = ");
  for (int i = 0; i <= P.deg; i++) {
    if (mpz_cmp_ui(P.coefs[i], 0) > 0) {
      if ((i != 0) && plusswitch) {
        printf(" + ");
      }
      if (i == 0) {
        mpz_out_str(stdout, 10, P.coefs[i]);
      } else {
        mpz_out_str(stdout, 10, P.coefs[i]);
        printf("x^%d", i);
      }
      plusswitch = 1;
    }
    exposant ++;
  }
  printf("\n");
  mpz_clear(coefprint);
  return 0;
}
