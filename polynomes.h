/* polynomes.h */



struct pol {
  uint deg;
  mpz_t* coefs;
};

typedef struct pol pol;


int print_bits_uint(uint n);
int pol_deg_from_coefs(pol P);
int pol_init(pol** P, uint deg);
int pol_clear(pol **P);
int pol_copy(pol *P_dest, pol *P_to_copy);
int pol_add_mod(pol* P_dest, pol* P, pol* Q, mpz_t n);
int pol_zero(pol* P);
int print_pol(pol P);
int pol_mult_mod_coef(pol *P_dest, pol *P, mpz_t coef, mpz_t n);
int pol_mult_mod_monome(pol *P_dest, pol *P, uint coef_monome, uint r);
int pol_multiplication_modulaire(pol* P_dest, pol* P, pol* Q, mpz_t n, uint r);
int pol_exponentiation_modulaire(pol* P_dest, pol* P, mpz_t n, uint r);
int exponentiation_modulaire_aks(pol* P_dest, mpz_t n, uint r, uint a);
