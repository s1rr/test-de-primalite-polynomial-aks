/* test.h */


int test_pol_cmp() {
  pol* P; pol_init(&P, 10);
  pol* Q; pol_init(&Q, 10);
  mpz_set_ui(P->coefs[0], 1);
  mpz_set_ui(Q->coefs[0], 1);
  mpz_set_ui(Q->coefs[9], 1);
  if (pol_cmp(P, Q)) {
    printf("Meme polynomes P et Q");
  } else {
    printf("Pols différents");
  }
  return 0;
}

int test_pol_add_mod() {
  uint r = 20;
  printf("test\n");
  mpz_t n;
  mpz_init_set_ui(n, 30);
  pol* P; pol_init(&P, r);
  pol* Q; pol_init(&Q, r);
  pol* P_res; pol_init(&P_res, r);

  pol_add_mod(P_res, P, Q, n);
  print_pol(*P_res);
  pol_zero(P_res);

  mpz_set_ui(P->coefs[0], 0);
  mpz_set_ui(Q->coefs[0], 30);
  pol_add_mod(P_res, P, Q, n);
  print_pol(*P_res);
  pol_zero(P_res);

  pol_clear(&P);
  pol_clear(&Q);
  pol_clear(&P_res);
  return 0;
}


int test_pol_mult_mod_coef() {
  uint rr = 20;
  mpz_t r;
  mpz_init_set_ui(r, rr);
  mpz_t coef;
  mpz_init_set_ui(coef, 22);
  pol* P;
  pol* P_res;
  pol_init(&P, rr - 1); // attention, on est sur des pol de deg r - 1
  pol_init(&P_res, rr - 1);

  for (int i = 0; i < rr; i++) { // Test P_res, P
    if (i != 0)
      mpz_set_ui(P->coefs[i - 1], 0);
    mpz_set_ui(P->coefs[i], 1);
    pol_mult_mod_coef(P_res, P, coef, r);
    // print_pol(*P);
    print_pol(*P_res);
    pol_zero(P_res);
  }
  pol_zero(P);
  for (int i = 0; i <= rr; i++) { // Test P, P
    mpz_set_ui(P->coefs[i], 1);
    pol_mult_mod_coef(P, P, coef, r);
    print_pol(*P);
    mpz_set_ui(P->coefs[i], 0);
  }
  pol_clear(&P);
  pol_clear(&P_res);
  mpz_clear(coef);
  return 0;
}


int test_pol_mult_mod_monome() {
  uint rr = 20;
  uint coef_monome = 0;
  mpz_t r;
  mpz_init_set_ui(r, rr);
  pol* P;
  pol* P_res;
  pol_init(&P, rr - 1);
  pol_init(&P_res, rr - 1);

  for (int i = 0; i < rr; i++) { // Test P_res, P
    if (i != 0)
      mpz_set_ui(P->coefs[i - 1], 0);
    mpz_set_ui(P->coefs[i], 1);
    pol_mult_mod_monome(P_res, P, coef_monome, rr);
    // print_pol(*P);
    print_pol(*P_res);
    pol_zero(P_res);
  }

  pol_clear(&P);
  pol_clear(&P_res);
  return 0;
}


int test_pol_multiplication_modulaire() {
  uint rr = 20;
  mpz_t n;
  mpz_init_set_ui(n, 10);
  pol* P;
  pol* Q;
  pol* P_res;
  pol_init(&P, rr - 1);
  pol_init(&Q, rr - 1);
  mpz_set_ui(Q->coefs[1], 4); // Q = 4X
  mpz_set_ui(P->coefs[0], 1); // P = 1 + X^2
  mpz_set_ui(P->coefs[2], 5);
  pol_init(&P_res, rr - 1);
  print_pol(*Q);
  printf("-----\n");


  for (int i = 0; i < rr; i++) { // Test P_res, P
    if (i != 0)
    mpz_set_ui(P->coefs[i - 1], 0);
    mpz_set_ui(P->coefs[i], 3);
    pol_multiplication_modulaire(P_res, P, Q, n, rr);
    // print_pol(*P);
    print_pol(*P_res);
    pol_zero(P_res);
  }

  pol_clear(&P);
  pol_clear(&P_res);
  return 0;
}




int test_pol_exponentiation_modulaire() {
  uint r = 14;
  uint a = 43;
  mpz_t n;
  mpz_init_set_ui(n, 1823);
  pol* P;
  pol* P_res;
  pol_init(&P, r - 1);
  pol_init(&P_res, r - 1);
  mpz_set_ui(P->coefs[1], 1);
  mpz_set_ui(P->coefs[0], a);
  print_pol(*P);
  printf("-----\n");

  pol_exponentiation_modulaire(P_res, P, n, r);
  print_pol(*P_res);

  pol_clear(&P);
  pol_clear(&P_res);

  return 0;
}


int test_exponentiation_modulaire_aks() {
  uint r = 3;
  mpz_t n;
  uint a = 2;
  mpz_init(n);
  mpz_set_ui(n, 5);
  printf("n: ");
  mpz_out_str(stdout, 10, n);
  printf("\n");
  pol* P_res;
  pol_init(&P_res, r - 1);
  exponentiation_modulaire_aks(P_res, n, r, a);
  pol_clear(&P_res);


  mpz_clear(n);
  return 0;
}


// Pas utilisé:
// generer polynomes de 1 a r - 1
/* uint n = 20; */
/*   pol* liste_monomes[n]; */
/*   for (int i; i < n; i++) { */
/*     pol_init(&liste_monomes[i], n); */
/*   } */
/*   generateur_monomes(liste_monomes, n); */

/*   for (int i; i < n; i++) { */
/*     pol_clear(&liste_monomes[i]); */
/*   } */

/* int generateur_monomes(pol* liste_monomes[], uint r) { */
/*   for (int i; i < r; i++) { */
/*     pol_init(&liste_monomes[i], r); */
/*   } */
/*   for (int i = 0; i < r; i++) { */
/*     mpz_set_ui(liste_monomes[i]->coefs[i], 1); // 1*X^i de deg i */
/*     liste_monomes[i]->deg = i; */
/*     //print_pol(*liste_monomes[i]); */
/*   } */
/*   return 0; */
/* } */
